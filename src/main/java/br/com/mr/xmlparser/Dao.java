package br.com.mr.xmlparser;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
@SuppressWarnings("unchecked")
public class Dao <T>{

	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	protected Class<T> clazz;

	public Dao(Class<T> clazz){
		this.clazz = clazz;
	}

	public void save(T linha) {
		getSession().save(linha);
	}

	
	public List<T> findAll() {
		Criteria criteria = getSession().createCriteria(clazz);
		return criteria.list();
	}
	
	public T find(Serializable key) {
		return (T) getSession().get(clazz, key);
	}

	public void update(T linha) {
		getSession().update(linha);
	}

	public Session getSession() {
		return getSessionFactory().getCurrentSession();
	}

	public List<T> findByParameter(String field, String value) {
		Criteria criteria = getSession().createCriteria(clazz);
		criteria.add(Restrictions.eq(field, value));
		return criteria.list();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
