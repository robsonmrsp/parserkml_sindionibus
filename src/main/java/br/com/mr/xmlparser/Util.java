package br.com.mr.xmlparser;

import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.PrecisionModel;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Folder;
import de.micromata.opengis.kml.v_2_2_0.LineString;
import de.micromata.opengis.kml.v_2_2_0.Placemark;

public class Util {
	public static List<String> getLinhas(Ponto p) {
		// linha(s)<br> 06; 11; 052; 056; 070; 092; 101]]>
		List<String> list = new ArrayList<String>();

		try {
			String str = p.getDescription();
			String[] linhas = null;
			int ini = str.indexOf("linha(s)<br>") + 12;
			linhas = str.substring(ini).split(" ");

			for (String string : linhas) {
				String replace = string.replace(";", "").replace(" ", "");
				if (!replace.isEmpty()) {
					list.add(replace);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(p.getDescription());
		}
		return list;
	}

	public static com.vividsolutions.jts.geom.LineString getLineString(LineString lineString) {
		List<Coordinate> coordinates = lineString.getCoordinates();
		com.vividsolutions.jts.geom.Coordinate[] coordinatesVet = new com.vividsolutions.jts.geom.Coordinate[coordinates.size()];
		GeometryFactory factory = new GeometryFactory(new PrecisionModel(), 4326);

		for (int i = 0; i < coordinates.size(); i++) {
			coordinatesVet[i] = new com.vividsolutions.jts.geom.Coordinate(coordinates.get(i).getLongitude(), coordinates.get(i).getLatitude());
		}
		return factory.createLineString(coordinatesVet);
	}

	public static List<Placemark> trataFolder(Feature feature) {
		List<Placemark> list = new ArrayList<Placemark>();
		if (feature instanceof Folder) {
			Folder folder = (Folder) feature;
			List<Feature> features = folder.getFeature();
			for (Feature fe : features) {
				if (fe instanceof Folder) {
					list.addAll(trataFolder(fe));
				} else if (fe instanceof Placemark) {
					list.add((Placemark) fe);
				}
			}
		}
		return list;
	}
}
