package br.com.mr.xmlparser;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.vividsolutions.jts.geom.Geometry;

@Entity
@Table(name = "tb_linha")
public class Linha implements Serializable {
	private static final long serialVersionUID = 5812098353962561764L;

	@Id
	@GeneratedValue(generator = "linha_seq_id")
	@SequenceGenerator(name = "linha_seq_id", sequenceName = "linha_seq_id")
	private Integer id;

	@Column(name = "short_name")
	private String shortName;

	@Column(name = "name")
	private String name;

	@Column(name = "type")
	private String type;

	@Column(name = "description")
	private String description;

	@Column(name = "the_geom")
	@Type(type = "org.hibernatespatial.GeometryUserType")
	private Geometry theGeom;

	@ManyToMany
	@JoinTable(name = "ponto_linha")
	private Set<Ponto> pontos;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Geometry getTheGeom() {
		return theGeom;
	}

	public void setTheGeom(Geometry theGeom) {
		this.theGeom = theGeom;
	}

	public Set<Ponto> getPontos() {
		if (pontos == null) {
			pontos = new HashSet<Ponto>();
		}
		return pontos;
	}

	public void addPonto(Ponto ponto) {
		getPontos().add(ponto);
	}

	public void setPontos(Set<Ponto> pontos) {
		this.pontos = pontos;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Linha [shortName=" + shortName + ", name=" + name + ", type=" + type + ", description=" + description + ", theGeom=" + theGeom + "]";
	}

}
