package br.com.mr.xmlparser;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

public class HibernateUtil {

	private static SessionFactory sessionFactory;

	static{
		sessionFactory = new Configuration()

		.setProperty(Environment.DIALECT, "org.hibernatespatial.postgis.PostgisDialect")
		.setProperty(Environment.DRIVER, "org.postgresql.Driver")
		.setProperty(Environment.URL, "jdbc:postgresql://localhost:5432/gisapp")

		.setProperty(Environment.USER, "postgres")
		.setProperty(Environment.PASS, "")

		.setProperty(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread")
		.setProperty(Environment.HBM2DDL_AUTO, "update")
		.setProperty(Environment.SHOW_SQL, "true")
		.setProperty(Environment.FORMAT_SQL, "true")
		.addAnnotatedClass(Linha.class)
		.addAnnotatedClass(Ponto.class)

		.buildSessionFactory();
	}

	public static SessionFactory getSessionFactory(){
		return sessionFactory;
	}

	public static Session getSession(){
		return sessionFactory.getCurrentSession();
	}

}
