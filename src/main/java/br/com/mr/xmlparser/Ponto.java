package br.com.mr.xmlparser;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.vividsolutions.jts.geom.Geometry;

@Entity
@Table(name = "tb_ponto")
public class Ponto {

	@Id
	@GeneratedValue(generator = "point_seq_id")
	@SequenceGenerator(name = "point_seq_id", sequenceName = "point_seq_id")
	public Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "description", length = 2048)
	private String description;

	@Column(name = "the_geom")
	@Type(type = "org.hibernatespatial.GeometryUserType")
	private Geometry theGeom;

	@ManyToMany(mappedBy = "pontos")
	private Set<Linha> linhas;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Geometry getTheGeom() {
		return theGeom;
	}

	public void setTheGeom(Geometry theGeom) {
		this.theGeom = theGeom;
	}

	public Set<Linha> getLinhas() {
		if (linhas == null) {
			linhas = new HashSet<Linha>();
		}
		return linhas;
	}

	public void setLinhas(Set<Linha> linhas) {
		this.linhas = linhas;
	}

	@Override
	public String toString() {
		return "Ponto [name=" + name + ", description=" + description + ", theGeom=" + theGeom + "]";
	}

	public void addLinha(Linha l) {
		getLinhas().add(l);
	}
}
