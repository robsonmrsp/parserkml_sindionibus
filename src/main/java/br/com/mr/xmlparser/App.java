package br.com.mr.xmlparser;

/**
 * Hello world!
 * 
 */
public class App {
	public static void main(String[] args) {
		try {

			// TODO não esquecer de configurar o banco de dados com o postgis!

			// Deve criar os pontos no banco e dados
			CriaPontosExec.exec();
			// Deve criar as linhas no bando de dados
			CriaLinhasExec.exec();

			// Uma vez criados os pontos e linhas, faz as devidas associações.
			AssociaLinhaPontoExec.exec();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
