package br.com.mr.xmlparser;

import java.io.IOException;
import java.util.List;

import org.hibernate.Session;
import org.jdom.JDOMException;

import com.vividsolutions.jts.geom.GeometryFactory;

import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Folder;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.Placemark;
import de.micromata.opengis.kml.v_2_2_0.Point;

public class CriaPontosExec {

	// replace br por \n
	public static void exec() throws JDOMException, IOException {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();

		final Kml kml = Kml.unmarshal(CriaPontosExec.class.getClassLoader().getResourceAsStream("pontos.kml"));
		final Document document = (Document) kml.getFeature();
		List<Feature> folders = document.getFeature();
		for (int i = 0; i < folders.size(); i++) {
			if (folders.get(i) instanceof Folder) {
				Folder folder = (Folder) folders.get(i);
				List<Feature> placemarks = folder.getFeature();
				for (int j = 0; j < placemarks.size(); j++) {
					if (placemarks.get(j) instanceof Placemark) {
						Placemark placemark = (Placemark) placemarks.get(j);
						Point point = (Point) placemark.getGeometry();
						placemark.getDescription();
						Ponto ponto = new Ponto();
						ponto.setName(placemark.getName());
						ponto.setDescription(placemark.getDescription());
						ponto.setTheGeom(new GeometryFactory().createPoint(new com.vividsolutions.jts.geom.Coordinate(point.getCoordinates().get(0).getLongitude(), point.getCoordinates().get(0).getLatitude())));
						session.save(ponto);
					}
				}
			}
		}
	}

}