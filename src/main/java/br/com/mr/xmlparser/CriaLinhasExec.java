package br.com.mr.xmlparser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.jdom.JDOMException;

import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.LineString;
import de.micromata.opengis.kml.v_2_2_0.Placemark;

public class CriaLinhasExec {

	public static void exec() throws JDOMException, IOException {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();

		final Kml kml2 = Kml.unmarshal(CriaPontosExec.class.getClassLoader().getResourceAsStream("linhas.kml"));
		final Document document2 = (Document) kml2.getFeature();
		List<Feature> folders2 = document2.getFeature();
		List<Placemark> placemarks = new ArrayList<Placemark>();

		for (int i = 0; i < folders2.size(); i++) {
			placemarks.addAll(Util.trataFolder(folders2.get(i)));
		}

		for (Placemark placemark : placemarks) {
			String name = placemark.getName();
			String shortName = name.substring(0, name.indexOf("-")).trim();
			String type = name.substring(name.lastIndexOf("-") + 1).trim();

			Linha linha = new Linha();
			linha.setName(placemark.getName());
			linha.setDescription(placemark.getName());
			linha.setShortName(shortName);
			linha.setType(type);
			LineString string = (LineString) placemark.getGeometry();

			com.vividsolutions.jts.geom.LineString lineString = Util.getLineString(string);
			linha.setTheGeom(lineString);
			session.save(linha);
			System.out.println(linha);
		}
	}

}
