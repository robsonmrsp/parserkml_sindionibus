package br.com.mr.xmlparser;

import java.io.IOException;
import java.util.List;

import org.jdom.JDOMException;

public class AssociaLinhaPontoExec {

	public static void exec() throws JDOMException, IOException {
		Dao<Linha> daoLinha = new Dao<Linha>(Linha.class);
		Dao<Ponto> daoPonto = new Dao<Ponto>(Ponto.class);

		HibernateUtil.getSession().beginTransaction();

		for (Ponto p : daoPonto.findAll()) {
			for (String str : Util.getLinhas(p)) {
				List<Linha> all = daoLinha.findByParameter("shortName", str);
				for (Linha l : all) {
					l.addPonto(p);
					p.addLinha(l);
				}
			}
		}

		HibernateUtil.getSession().getTransaction().commit();
	}

}
